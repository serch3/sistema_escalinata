<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('escalinata', function (Blueprint $table) {
            $table->id();
            $table->text('carnet_identidad');
            $table->text('nombre_completo');
            $table->text('primer_apellido');
            $table->text('segundo_apellido');
            $table->text('celular');
            $table->text('correo_electronico')->nullable();
            $table->text('contenido_de_la_placa');
            $table->text('baucher')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('escalinata');
    }
};
