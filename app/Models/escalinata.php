<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class escalinata extends Model
{
    use HasFactory;
    protected $table = 'escalinata';
    protected $fillable = [
        'carnet_identidad',
        'nombre_completo',
        'primer_apellido',
        'segundo_apellido',
        'celular',
        'correo_electronico',
        'contenido_de_la_placa',
        'baucher',
    ];
}
