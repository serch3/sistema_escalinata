<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\escalinata;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use DateTime;



class EscalinataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('home');
        //return view('pages.billing')->with(['dato'=>$dato,'cantidad'=>$cantidad]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $formulario= new escalinata;
        $formulario->carnet_identidad= $request->input('carnet_identidad');
        $formulario->nombre_completo= $request->input('nombre_completo');
        $formulario->primer_apellido= $request->input('primer_apellido');
        $formulario->segundo_apellido= $request->input('segundo_apellido');
        $formulario->celular= $request->input('celular');
        $formulario->correo_electronico= $request->input('correo_electronico');
        $formulario->contenido_de_la_placa= $request->input('contenido_de_la_placa');
        $formulario->baucher= $request->input('baucher');
        $formulario->save();
    }

    public function validateForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'carnet_identidad'=>'required',
            'nombre_completo' => 'required',
            'primer_apellido' => 'required',
            'segundo_apellido' => 'required',
            'celular'=>'required',
            'contenido_de_la_placa' => 'required|max:70',
            //'baucher'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        // La validación fue exitosa, puedes realizar alguna acción adicional si es necesario
        $formulario= new escalinata;
        $formulario->carnet_identidad= $request->input('carnet_identidad');
        $formulario->nombre_completo= $request->input('nombre_completo');
        $formulario->primer_apellido= $request->input('primer_apellido');
        $formulario->segundo_apellido= $request->input('segundo_apellido');
        $formulario->celular= $request->input('celular');
        $formulario->correo_electronico= $request->input('correo_electronico');
        $formulario->contenido_de_la_placa= $request->input('contenido_de_la_placa');
        $formulario->baucher= $request->input('baucher');
        $formulario->save();
        return response()->json(['success' => true,'formulario'=>$formulario]);
    }

    public function pdf(Request $request){
        $solicitudId = $request->id;    
        $solicitud = escalinata::find($solicitudId);
        $informe=DB::table('escalinata')->where('escalinata.id','=',$solicitudId)
            ->select('escalinata.carnet_identidad','escalinata.nombre_completo','escalinata.primer_apellido','escalinata.segundo_apellido','escalinata.celular','escalinata.correo_electronico','escalinata.contenido_de_la_placa','escalinata.baucher')
            -> first();         
        //configuracion pdf
        $vista = view('pdf', [
            'informe'        =>  $informe,
        ]);
         

        $options = new Options(); 
        $options->set('isRemoteEnabled', TRUE);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($vista);
        $dompdf->setPaper('legal', 'portrait');
        $dompdf->set_option('isPhpEnabled', true);
        //$dompdf->page_text(1,1, "{PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // page_text($w - 120, $h - 40, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
        $dompdf->render();
        // $dompdf->stream('autorizaciones.pdf');
        $dompdf->stream ('',array("Attachment" => false));
    
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
