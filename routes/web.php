<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

use App\Http\Controllers\EscalinataController;
Route::get('/home', [EscalinataController::class, 'index'])->name('home');
Route::post('/formulario', [EscalinataController::class, 'store'])->name('guardar_formulario');
Route::post('/Validate', [EscalinataController::class, 'validateForm'])->name('validateForm');
Route::post('/descargar-formulario', [EscalinataController::class, 'pdf'])->middleware('auth')->name('descargarpdf');



Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
