<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css"  />


<body>
  <header>
    <div class="company-logo">Logo</div>
    <nav class="navbar">
      <ul class="nav-items">
        <li class="nav-item"><a href="#" class="nav-link">HOME</a></li>
        <li class="nav-item"><a href="#" class="nav-link">OFFER</a></li>
      </ul>
    </nav>
    <div class="menu-toggle">
      <i class="bx bx-menu"></i>
      <i class="bx bx-x"></i>
    </div>
  </header>
  <main>
    <!-- HOME SECTION -->
      <div class="slogan">
       <!-- <h1 class="company-title">DOWNTOWN TECH LIVER</h1>
        <h2 class="company-slogan">
          The biggest computer shop in the area where customers come first.
        </h2>-->
      </div>
        <img class="home-computer" src="/img/slider.jpg" alt="a computer in dark with shadow" class="home-img">
      
    <!-- OFFER SECTION -->
    <div id="div_formulario">
        <!--Formulario -->
            <!-- multistep form -->
            <form id="msform" action="{{ route('guardar_formulario') }}" method="post">
                @csrf
                <!-- progressbar -->
                <ul id="progressbar">
                <li class="active">Account Setup</li>
                <li>Social Profiles</li>
                <li>Personal Details</li>
                </ul>
                <!-- fieldsets -->
                <fieldset>
                <h2 class="fs-title">Datos Solicitante</h2>
                <h3 class="fs-subtitle">Mejoramiento Escalinatas Cristo De La Concordia</h3>
                <input type="text" name="carnet_identidad" placeholder="Documento de Identidad" />
                <input type="text" name="nombre_completo" placeholder="Nombre Completo" />
                <input type="text" name="primer_apellido" placeholder="Primer Apellido" />
                <input type="text" name="segundo_apellido" placeholder="Segundo Apellido" />
                <input type="text" onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" name="celular" placeholder="Numero Celular" />
                <!--<input type="text" name="correo_electronico" placeholder="Correo Electronico" />-->
                <input type="button" name="next" class="next action-button" value="Siguiente" />
                </fieldset>
               <fieldset>
                <h2 class="fs-title">Contenido De La Placa</h2>
                <h3 class="fs-subtitle">Mejoramiento Escalinatas Cristo De La Concordia</h3>
                <input type="text" name="contenido_de_la_placa" placeholder="nombre de la placa" />
                <input type="button" name="previous" class="previous action-button" value="Atras" />
                <input id="btn_registrar" type="submit"  value="Registrar" class="submit action-button">
                <div id="nombreError">
                </div>
                </fieldset>
                <fieldset>
                <h2 class="fs-title">Adjuntar Boucher</h2>
                <h3 class="fs-subtitle">Mejoramiento Escalinatas Cristo De La Concordia</h3>
                <div id="div_comprobante" class="text-center">
                    <div class="input_file">
                    <label class="text-gray"><span class="text-danger">(*)</span>Comprobante De Pago</label>
                    <i class="icon image fa fa-photo la-2x" aria-hidden="true"></i>
                    <div class="col-sm-12 col-md-12 col-lx-12 file-loading">
                        {{-- file upload dropzone muestra las fotos del local --}}
                        <div id="comprobante" class="dropzone">
                        </div>
                        <input type="hidden" id="baucher" name="baucher" placeholder="boucher" />

                        <br>
                        {{-- end upload image --}}
                    </div>
                    </div>
                </div>

                <input id="btn_atras" type="button" name="previous" class="previous action-button" value="Atras" />
                
                <span id="nombreError" class="error"></span>

                </fieldset>
            </form>
      <!--Fin Formulario-->
    </div>
    <!--boton descargar pdf-->
    <div id="div_botonpdf" class="text-center">
        <form action="{{route('descargarpdf')}}" method="post" target="_blank">
            @csrf 
            <input type="hidden" name="id" id="myInput">
            <button id="btn_pdf_" class="custom-btn btn-5"><span>Descargar pdf</span></button>
        </form>
    </div>
    <!--fin boton descargar pdf-->    
    
    <!--<section class="container section-2">
       
      <div class="offer offer-1">
        <img src="https://github.com/r-e-d-ant/Computer-store-website/blob/main/assets/images/offer_1.png?raw=true" alt="a computer in dark with with white shadow" class="offer-img offer-1-img">
        <div class="offer-description offer-desc-1">
          <h2 class="offer-title">Macbook pro</h2>
          <p class="offer-hook">This a Macbook pro nulla vulputate magna vel odio sagittis bibendium...</p>
          <div class="cart-btn">ADD TO CART</div>
        </div>
        
      </div>
      --Formulario 
             multistep form 
            <form id="msform">
             progressbar
            <ul id="progressbar">
            <li class="active">Account Setup</li>
            <li>Social Profiles</li>
            <li>Personal Details</li>
            </ul>
             fieldsets 
            <fieldset>
            <h2 class="fs-title">Create your account</h2>
            <h3 class="fs-subtitle">This is step 1</h3>
            <input type="text" name="email" placeholder="Email" />
            <input type="password" name="pass" placeholder="Password" />
            <input type="password" name="cpass" placeholder="Confirm Password" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Social Profiles</h2>
            <h3 class="fs-subtitle">Your presence on the social network</h3>
            <input type="text" name="twitter" placeholder="Twitter" />
            <input type="text" name="facebook" placeholder="Facebook" />
            <input type="text" name="gplus" placeholder="Google Plus" />
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Personal Details</h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <input type="text" name="fname" placeholder="First Name" />
            <input type="text" name="lname" placeholder="Last Name" />
            <input type="text" name="phone" placeholder="Phone" />
            <textarea name="address" placeholder="Address"></textarea>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <a href="https://twitter.com/GoktepeAtakan" class="submit action-button" target="_top">Submit</a>
            </fieldset>
            </form>
      Fin Formulario
        
    
    </section>-->

  </main>
  <!--<footer>
    <div class="container top-footer">
      <div class="footer-item">
        <h2 class="footer-title">ADDRESS</h2>
        <div class="footer-items">
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing st18</h3>
        </div>
      </div>
      
      <div class="footer-item">
        <h2 class="footer-title">SERVICES</h2>
        <div class="footer-items">
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
        </div>
      </div>
      <div class="footer-item">
        <h2 class="footer-title">SUPPLIERS</h2>
        <div class="footer-items">
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
        </div>
      </div>
      <div class="footer-item">
        <h2 class="footer-title">INVESTMENT</h2>
        <div class="footer-items">
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
          <h3>Adipisicing elit.</h3>
        </div>
      </div>
    </div>
    <div class="container end-footer">
      <div class="copyright">copyright © 2021 - Present • <b>DOWNTOWN TECH LIVER</b></div>
      <a class="designer" href="#">Thierry M</a>
    </div>
  </footer>-->
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js" ></script>

<script>
    //Comprobante de pago 
    var dropzoneeliminar= $("#comprobante").dropzone({
    dictDefaultMessage: "Arrastre y suelte aquí los archivos …<br>(o haga clic para seleccionar archivos)",
    dictRemoveFile: 'Remover Archivo',
    dictCancelUpload: 'Cancelar carga',
    dictResponseError: 'Server responded with  code.',
    dictCancelUploadConfirmation: '¿Estás seguro/a de que deseas cancelar esta carga?',
    url: "https://repositoriogamc.cochabamba.bo/api/v1/repository/upload-files",
    paramName: "documens_files[]",
    addRemoveLinks: true,
    acceptedFiles: '.pdf',
    parallelUploads: 1,
    maxFiles: 1,
    maxFilesize:100,
    init: function() {
        this.on("error", function (file, message) {
                        Swal.fire(message);
                        
                    }); 
        this.on("complete", function(file) {
        if (!file.type.match('.pdf')) {
            this.removeFile(file);
            Swal.fire('No se puede subir el archivo ' + file.name)
            return false;
        }
        });

        this.on("removedfile", function(file) {
        console.log('removiendo archivo ' + file.name);
        $('#baucher').val('');
        });

        this.on("maxfilesexceeded", function(file) {
        file.previewElement.classList.add("dz-error");
        $('.dz-error-message').text('No se puede subir mas archivos!');
        });

    },
    sending: function(file, xhr, formData) {
        formData.append('sistema_id', '00e8a371-8927-49b6-a6aa-0c600e4b6a19');
        formData.append('collector', 'upload files events');
    },
    success: function(file, response) {
        file.previewElement.classList.add("dz-success");
        $('#baucher').val(response.response[0].url_file);
    },
    error: function(file, response) {
        file.previewElement.classList.add("dz-error");
        $('.dz-error-message').text('No se pudo subir el archivo ' + file.name);
    }
    });
</script>


<!--Validacion de los datos-->
<!--Finn-->
<script>
    $(document).ready(function() {
        document.getElementById("div_botonpdf").style.display= 'none'
        $('#msform').submit(function(event) {
            event.preventDefault(); // Evita el envío del formulario por defecto

            var formData = $(this).serialize(); // Obtén los datos del formulario

            $.ajax({
                url: '{{ route('validateForm') }}', // Ruta a tu controlador Laravel para validar el formulario
                type: 'POST',
                data: formData,
                success: function(response) {
                    Swal.fire({title: 'Registro Guardado',    icon: 'success',html:'Su formulario esta listo para impresion',showCloseButton: true,focusConfirm: false,})
                    document.getElementById("btn_atras").style.display= 'none'
                    document.getElementById("btn_registrar").style.display= 'none'
                    document.getElementById("div_formulario").style.display= 'none'
                    document.getElementById("div_botonpdf").style.display= ''
                    var id_guardar_pdf = response.formulario.id; 
                    var myInput = document.getElementById("myInput");
                    myInput.value = id_guardar_pdf;
                },
                error: function(xhr) {
                    let diverrors = ""
                    var errors = xhr.responseJSON.errors; // Obtiene los errores de validación
                    console.log(errors);
                    Object.keys(errors).forEach(function(key) {
                        diverrors +=`<p> ${errors[key]}</p><br>`;
                    });
                    document.getElementById('nombreError').innerHTML = diverrors


                }
            });
        });
    });
</script>


<!-- Fin Guardar dato en la base de datos-->
<!--Estilos del home css-->
<style>
    @import url("https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400");

        *,
        *::before,
        *::after {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        }

        /* ================= VARIABLE ================ */
        :root {
        --primary-color: hsl(9, 94%, 61%);
        --primary-color-alt: hsl(28, 72%, 83%);
        --second-color: #3e537c;
        --second-color-alt: hsla(220, 33%, 36%, 65%);
        --third-color: hsl(220, 36%, 28%);
        --white-color: #fbfbfb;
        --white-color-alt: hsl(12, 14%, 93%);
        --dark-color: hsl(300, 100%, 0%);
        }

        /* ================= BASE ==================== */
        li {
        list-style: none;
        }
        a {
        text-decoration: none;
        }
        img {
        width: 100%;
        height: auto;
        }
        .bx {
        font-size: 2.5rem;
        }
        .container {
        padding: 0 2rem;
        }

        /* -- BODY -- */
        body {
        font-family: "Raleway", sans-serif;
        font-size: 1rem;
        background-color: var(--white-color);
        }

        /* ================= HEADER ================ */
        header {
        display: flex;
        align-items: center;
        justify-content: space-between;
        background-color: var(--dark-color);
        padding: 1rem 2rem;
        }
        .company-logo {
        font-size: 2.5rem;
        background: -webkit-linear-gradient(
            120deg,
            var(--primary-color-alt),
            var(--primary-color)
        );
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        }
        .nav-items {
        display: flex;
        }
        .nav-item {
        margin: 0 2rem;
        }
        .nav-link {
        font-size: 1.1rem;
        letter-spacing: 0.05rem;
        position: relative;
        background: -webkit-linear-gradient(
            var(--primary-color-alt),
            var(--primary-color)
        );
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        }
        .nav-link::before {
        content: "";
        background: linear-gradient(var(--primary-color), var(--primary-color-alt));
        width: 100%;
        height: 0.05rem;
        position: absolute;
        left: 0;
        bottom: 0;
        transform: scaleX(0);
        transform-origin: bottom right;
        transition: transform 150ms;
        }
        .nav-link:hover::before {
        transform: scaleX(1);
        transform-origin: bottom left;
        }
        .menu-toggle {
        display: none;
        }
        .bx-menu,
        .bx-x {
        cursor: pointer;
        background: -webkit-linear-gradient(
            120deg,
            var(--primary-color-alt),
            var(--primary-color)
        );
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        display: none !important;
        }

        /* ================= MAIN ================ */

        /* -------- HOME SECTION -------------- */
        .section-1 {
        background-color: var(--dark-color);
        display: grid;
        }
        /* .home-computer-container {} */

        .slogan .company-title {
        background: -webkit-linear-gradient(
            120deg,
            var(--primary-color-alt),
            var(--primary-color)
        );
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        margin-top: 2rem;
        font-size: 1.5rem;
        font-weight: 600;
        }
        .slogan .company-slogan {
        background: -webkit-linear-gradient(
            120deg,
            var(--primary-color-alt),
            var(--primary-color)
        );
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        margin-top: 1rem;
        font-size: 1.2rem;
        font-weight: 400;
        line-height: 1.8rem;
        }

        /* -------- OFFER SECTION ------------- */
        .section-2 {
        margin: 2rem 0;
        }
        .offer {
        background-color: var(--dark-color);
        margin: 1rem 0;
        display: grid;
        }
        .offer img {
        background-color: var(--dark-color);
        padding: 2rem 0;
        }
        .offer-1 img {
        background-color: var(--dark-color);
        padding: 3rem 0;
        }
        .offer-description {
        background-color: var(--white-color-alt);
        padding: 0 1rem;
        }
        .offer-description .offer-title {
        color: var(--second-color);
        font-size: 1.8rem;
        font-weight: 400;
        padding: 1.5rem 0 0.5rem 0;
        }
        .offer-description .offer-hook {
        color: var(--second-color-alt);
        font-size: 1.2rem;
        }
        .offer-description .cart-btn {
        cursor: pointer;
        color: var(--second-color-alt);
        font-size: 1.1rem;
        display: grid;
        place-items: center;
        margin: 2rem 0 1rem 0;
        width: 10rem;
        height: 3rem;
        background-image: linear-gradient(
            to top,
            var(--primary-color-alt),
            var(--primary-color)
        );
        }
        .offer-description .cart-btn:hover {
        background-image: linear-gradient(
            to bottom,
            var(--primary-color-alt),
            var(--primary-color)
        );
        }

        /* -------- PRODUCT SECTION ----------- */
        .section-3 {
        display: grid;
        place-items: center;
        justify-content: space-around;
        gap: 1rem;
        grid-template-columns: repeat(auto-fit, minmax(200px, 400px));
        }
        .product {
        cursor: pointer;
        background-color: var(--white-color-alt);
        position: relative;
        }
        .product::before {
        content: "";
        background-image: linear-gradient(
            to bottom,
            hsla(29, 72%, 83%, 0.438),
            hsla(9, 94%, 61%, 0.507)
        );
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        }
        .product_add_cart {
        color: var(--white-color-alt);
        font-size: 1.2rem;
        background-color: hsl(9, 94%, 65%);
        padding: 1rem 0.4rem;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        transition: transform 300ms, color 300ms, box-shadow 300ms;
        }
        .product_add_cart:hover {
        color: var(--white-color);
        box-shadow: 0 1rem 0 -0.5rem hsl(17, 79%, 65%, 0.7),
            0 2rem 0 -1rem hsla(17, 79%, 65%, 0.65);
        }

        /* -------- SPONSOR SECTION ----------- */
        .section-4 {
        margin: 4rem 0;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: space-around;
        }
        .sponsor img {
        cursor: pointer;
        width: 40px;
        height: 40px;
        filter: grayscale(100%);
        opacity: 0.8;
        transition: opacity 300ms;
        }
        .sponsor img:hover {
        /* filter: grayscale(0); */
        opacity: 1;
        }

        /* ========= SUBSCRIBE SECTION ========== */
        .section-5 {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        margin: 2rem 0;
        }
        .subscribe-input-label {
        margin-bottom: 1rem;
        font-size: 1.5rem;
        font-weight: 400;
        letter-spacing: 0.05rem;
        color: var(--second-color);
        }
        input[type="text"] {
        padding: 0 0.5rem;
        font-size: 1.1rem;
        width: 100%;
        height: 3rem;
        border: none;
        background-color: hsl(220, 33%, 90%);
        color: var(--second-color-alt);
        transition: background-color 300ms;
        }
        input[type="text"]:focus {
        outline: none;
        background-color: hsl(220, 33%, 95%);
        }
        input[type="text"]::placeholder {
        color: var(--second-color-alt);
        }
        input[type="submit"] {
        cursor: pointer;
        background-image: linear-gradient(
            to top,
            var(--primary-color-alt),
            var(--primary-color)
        );
        color: var(--white-color-alt);
        margin: 1rem 0;
        border: none;
        width: 100%;
        height: 3rem;
        font-size: 1.2rem;
        transition: color 300ms;
        }
        input[type="submit"]:hover {
        background-image: linear-gradient(
            to bottom,
            var(--primary-color-alt),
            var(--primary-color)
        );
        color: var(--white-color);
        }
        /* =============== FOOTER =============== */
        .top-footer {
        background-color: var(--second-color);
        display: flex;
        flex-direction: column;
        }
        .footer-title {
        font-weight: 500;
        font-size: 1.2rem;
        padding: 1rem 0;
        background-image: -webkit-linear-gradient(
            120deg,
            var(--primary-color-alt),
            var(--primary-color)
        );
        background-clip: text;
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        }
        .footer-items h3 {
        cursor: pointer;
        font-weight: 300;
        font-size: 1.1rem;
        padding: 0.1rem 0;
        color: var(--white-color-alt);
        }
        .footer-items h3:hover {
        text-decoration: underline;
        }
        .footer-items h3:last-child {
        padding-bottom: 2rem;
        }

        .end-footer {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        background-color: var(--third-color);
        }
        .copyright {
        color: var(--white-color-alt);
        padding: 1rem 0;
        font-size: 0.9rem;
        }
        .copyright b {
        font-weight: inherit;
        font-size: 0.9rem;
        }
        .designer {
        color: hsla(0, 0%, 98%, 0.541);
        padding-bottom: 0.5rem;
        font-size: 0.9rem;
        }
        .designer:hover {
        text-decoration: underline;
        }

        /* =============== MEDIA QUERIES ======= */

        @media screen and (max-width: 768px) {
        .container {
            padding: 0 1rem;
        }
        /* ================= HEADER ================ */
        header {
            padding: 0.5rem 1rem;
        }
        .navbar {
            background-color: var(--dark-color);
            position: absolute;
            top: 3.5rem;
            right: 0;
            width: 100%;
            height: 100vh;
            display: flex;
            justify-content: center;
            transform: scaleY(0);
            transform-origin: bottom;
            transition: transform 500ms;
        }
        .show-navbar {
            display: flex;
            transform: scaleY(1);
            transform-origin: top;
            transition: transform 300ms;
        }
        .nav-items {
            display: flex;
            flex-direction: column;
        }
        .nav-item {
            margin: 0.5rem 0;
        }
        .menu-toggle {
            display: block;
        }
        .bx-menu {
            display: block !important;
        }
        .show-bx {
            display: block !important;
        }
        .hide-bx {
            display: none !important;
        }
        }
        @media (min-width: 769px) {
        header {
            padding: 1rem 5rem;
        }
        /* ================= MAIN ================ */

        /* -------- HOME SECTION -------------- */
        .section-1 {
            justify-content: space-between;
            grid-template-columns: 1fr 1fr;
            padding: 10rem 5rem 0 5rem;
        }
        .slogan .company-title {
            max-width: 30rem;
            font-size: 1.8rem;
            letter-spacing: 0.5rem;
        }
        .slogan .company-slogan {
            max-width: 20rem;
            font-size: 1.3rem;
        }
        /* -------- OFFER SECTION ------------- */
        .section-2 {
            margin: 2rem 5rem;
        }
        .offer {
            margin: 5rem 0;
            align-items: center;
            justify-content: space-between;
            grid-template-rows: auto auto;
        }
        .offer-1 {
            grid-template-areas: "offer-1-img offer-desc-1";
        }
        .offer-1 img {
            background-color: var(--dark-color);
            padding: 2rem 0;
        }
        .offer-2 {
            grid-template-areas: "offer-desc-2 offer-2-img";
        }
        .offer-1-img {
            grid-area: offer-1-img;
        }
        .offer-2-img {
            grid-area: offer-2-img;
        }
        .offer-desc-1 {
            grid-area: offer-desc-1;
        }
        .offer-desc-2 {
            grid-area: offer-desc-2;
        }
        .offer-description .offer-title {
            font-size: 1.9rem;
            padding: 1.5rem 0 0.5rem 0;
        }
        .offer-description .offer-hook {
            max-width: 30rem;
        }
        .offer-description {
            background-color: var(--white-color-alt);
            padding: 2rem 1rem;
        }
        .offer-description .offer-title {
            padding: 0.5rem 0 1rem 0;
        }
        .offer-description .cart-btn {
            margin: 2rem 0 0.5rem 0;
        }
        /* -------- PRODUCT SECTION ----------- */
        .section-3 {
            gap: 5rem;
        }
        .product::before {
            transform: scaleY(0);
            transform-origin: bottom;
            transition: transform 300ms;
        }
        .product:hover::before {
            transform: scaleY(1);
            transform-origin: top;
            transition: transform 300ms;
        }
        .product_add_cart {
            transform: scaleY(0);
            transform-origin: bottom;
        }
        .product:hover .product_add_cart {
            transform: translate(-50%, -50%) scaleY(1);
            transform-origin: top;
        }
        /* -------- SPONSOR SECTION ----------- */
        .section-4 {
            margin: 5rem 0;
        }
        /* ========= SUBSCRIBE SECTION ========== */
        .section-5 {
            padding: 1rem 0;
        }
        .section-5 .subscribe-container {
            display: flex;
            align-items: center;
        }
        input[type="text"] {
            padding: 0 1rem;
            width: 15rem;
        }
        input[type="submit"] {
            width: 10rem;
        }
        /* =============== FOOTER =============== */
        .top-footer {
            flex-direction: row;
            justify-content: space-around;
            padding-bottom: 8rem;
            padding-top: 2rem;
        }
        .footer-title {
            font-size: 1rem;
            padding: 1rem 0;
        }
        .footer-items h3 {
            font-size: 0.9rem;
        }

        .end-footer {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            background-color: var(--third-color);
        }
        .copyright {
            color: hsla(0, 0%, 98%, 0.747);
            padding: 1rem 0;
            font-size: 0.8rem;
        }
        .copyright b {
            font-size: 0.7rem;
        }
        .designer {
            color: hsla(0, 0%, 98%, 0.322);
            font-size: 0.8rem;
        }
        }

</style>
<!--javascript home-->
<script>    
    'use strict'

    const menuToggle = document.querySelector('.menu-toggle');
    const bxMenu = document.querySelector('.bx-menu');
    const bxX = document.querySelector('.bx-x');

    const navBar = document.querySelector('.navbar');

    // --- open menu ---

    bxMenu.addEventListener('click', (e)=> {
        if(e.target.classList.contains('bx-menu')){
            navBar.classList.add('show-navbar');
            bxMenu.classList.add('hide-bx');
            bxX.classList.add('show-bx');
        }
    })

    // --- close menu ---

    bxX.addEventListener('click', (e)=> {
        if(e.target.classList.contains('bx-x')){
            navBar.classList.remove('show-navbar');
            bxMenu.classList.remove('hide-bx');
            bxX.classList.remove('show-bx');
        }
    })


</script>
<style>
    /*custom font*/
        @import url(https://fonts.googleapis.com/css?family=Montserrat);

        /*basic reset*/
        * {margin: 0; padding: 0;}



        /*form styles*/
        #msform {
            width: 500px;
            margin: 50px auto;
            text-align: center;
            position: relative;
        }
        #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 3px;
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
            padding: 20px 30px;
            box-sizing: border-box;
            width: 80%;
            margin: 0 10%;
            
            /*stacking fieldsets above each other*/
        }
        /*Hide all except first fieldset*/
        #msform fieldset:not(:first-of-type) {
            display: none;
        }
        /*inputs*/
        #msform input, #msform textarea {
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 3px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
            font-family: montserrat;
            color: #2C3E50;
            font-size: 13px;
        }
        /*buttons*/
        #msform .action-button {
            width: 100px;
            background: #27AE60;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 1px;
            cursor: pointer;
            padding: 10px;
            margin: 10px 5px;
        text-decoration: none;
        font-size: 14px;
        }
        #msform .action-button:hover, #msform .action-button:focus {
            box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
        }
        /*headings*/
        .fs-title {
            font-size: 15px;
            text-transform: uppercase;
            color: #2C3E50;
            margin-bottom: 10px;
        }
        .fs-subtitle {
            font-weight: normal;
            font-size: 13px;
            color: #666;
            margin-bottom: 20px;
        }
        /*progressbar*/
        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            /*CSS counters to number the steps*/
            counter-reset: step;
        }
        #progressbar li {
            list-style-type: none;
            color: white;
            text-transform: uppercase;
            font-size: 9px;
            width: 33.33%;
            float: left;
            position: relative;
        }
        #progressbar li:before {
            content: counter(step);
            counter-increment: step;
            width: 20px;
            line-height: 20px;
            display: block;
            font-size: 10px;
            color: #333;
            background: white;
            border-radius: 3px;
            margin: 0 auto 5px auto;
        }
        /*progressbar connectors*/
        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: white;
            position: absolute;
            left: -50%;
            top: 9px;
            z-index: -1; /*put it behind the numbers*/
        }
        #progressbar li:first-child:after {
            /*connector not needed before the first step*/
            content: none; 
        }
        /*marking active/completed steps green*/
        /*The number of the step and the connector before it = green*/
        #progressbar li.active:before,  #progressbar li.active:after{
            background: #27AE60;
            color: white;
        }
</style>
<script>
    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();
        
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        
        //show the next fieldset
        next_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
            'transform': 'scale('+scale+')',
            'position': 'absolute'
        });
                next_fs.css({'left': left, 'opacity': opacity});
            }, 
            duration: 800, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        
        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
        //show the previous fieldset
        previous_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            }, 
            duration: 800, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });
</script>
<style>
        .custom-btn {
        width: 130px;
        height: 40px;
        color: #fff;
        border-radius: 5px;
        padding: 10px 25px;
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: transparent;
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
        7px 7px 20px 0px rgba(0,0,0,.1),
        4px 4px 5px 0px rgba(0,0,0,.1);
        outline: none;
        }
        .btn-5 {
        width: 130px;
        height: 40px;
        line-height: 42px;
        padding: 0;
        border: none;
        background: rgb(255,27,0);
        background: linear-gradient(0deg, rgba(255,27,0,1) 0%, rgba(251,75,2,1) 100%);
        }
        .btn-5:hover {
        color: #f0094a;
        background: transparent;
        box-shadow:none;
        }
        .btn-5:before,
        .btn-5:after{
        content:'';
        position:absolute;
        top:0;
        right:0;
        height:2px;
        width:0;
        background: #f0094a;
        box-shadow:
        -1px -1px 5px 0px #fff,
        7px 7px 20px 0px #0003,
        4px 4px 5px 0px #0002;
        transition:400ms ease all;
        }
        .btn-5:after{
        right:inherit;
        top:inherit;
        left:0;
        bottom:0;
        }
        .btn-5:hover:before,
        .btn-5:hover:after{
        width:100%;
        transition:800ms ease all;
        }

    </style>